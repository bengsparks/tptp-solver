import itertools
import os.path
from sys import argv
from typing import Iterable

from gavel.dialects.tptp.parser import TPTPParser
from gavel.logic.problem import Import, Problem

from sine import sine


def main(filepaths: list):
    parser = TPTPParser()
    structures = ((filepath, parser.parse_from_file(filepath)) for filepath in filepaths)

    for filepath, structure in structures:
        try:
            print(f"{filepath}: creating problem")
            problem = __create_problem_from_structure(structure)
            print(f"{filepath}: finished creating problem")
            print(f"{filepath}: applying sine")
            subset_problem = sine(problem)

            # for premise in subset_problem.premises:
            #     print(f"{premise}")
            # print(f"\n{subset_problem.conjecture}")

            print(f"Reduced problem from {len(problem.premises)} to {len(subset_problem.premises)} axioms")

        except TypeError as e:
            print(f"TF IS THIS???: {e}")


def __create_problem_from_structure(structure: Iterable, is_problem: bool = True) -> Problem:
    _formulas, _imports = __partition(
        lambda s: isinstance(s, Import), structure
    )

    parser = TPTPParser()
    extracted = map(lambda s: s.formula, _formulas)

    tptp_path = os.path.join(os.getcwd(), "TPTP-v7.3.0")
    imported_premises = map(
        lambda i: __create_problem_from_structure(
            parser.parse_from_file(os.path.join(tptp_path, i.path)),
            is_problem=False
        ).premises,
        _imports
    )

    formulas = list(itertools.chain(
        extracted,
        itertools.chain.from_iterable(imported_premises)
    ))
    # last formula is the conjecture in the problems
    if is_problem:
        return Problem(formulas[:-1], formulas[-1])
    # axioms have no goal
    return Problem(formulas, [])


def __partition(predicate, sequence):
    t1, t2 = itertools.tee(sequence)
    return itertools.filterfalse(predicate, t1), filter(predicate, t2)


if __name__ == '__main__':
    if len(argv) <= 1:
        raise Exception(f"usage: python {os.path.basename(__file__)} filepath [filepaths]")

    main(argv[1:])
