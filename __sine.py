import itertools
from collections import Counter
from functools import reduce
from typing import Iterable

import lark
from gavel.dialects.db.structures import Formula
from gavel.logic.logic import BinaryFormula, DefinedConstant, PredicateExpression, FunctorExpression, Constant, \
    UnaryFormula, Variable, QuantifiedFormula
from gavel.logic.problem import Problem
from tqdm import tqdm


def __count_symbols_in_axioms(problem: Problem) -> Counter:
    def __merger_helper(needle, haystack: Counter) -> int:
        if isinstance(needle, lark.lexer.Token):
            possible = filter(lambda k: isinstance(k, lark.lexer.Token), haystack.keys())
            match = next(filter(lambda k: str(k) == str(needle), possible), None)
        elif isinstance(needle, DefinedConstant):
            possible = filter(lambda k: isinstance(k, DefinedConstant), haystack.keys())
            match = next(filter(lambda k: str(k) == str(needle), possible), None)
        elif isinstance(needle, Constant):
            possible = filter(lambda k: isinstance(k, Constant), haystack.keys())
            match = next(filter(lambda k: k.symbol == needle.symbol, possible), None)
        else:
            raise Exception(f"unhandled type in {__merger_helper.__name__}: {type(needle)}")

        return haystack[match]

    def __merge_duplicates(acc: Counter, curr: Counter) -> Counter:
        _common = acc.keys() & curr.keys()
        _merged = Counter({
            symbol: acc.get(symbol) + curr.get(symbol)
            for symbol in _common
        })

        for remainder in acc.keys() - _merged.keys():
            _merged[remainder] += __merger_helper(remainder, curr) + acc[remainder]

        for remainder in curr.keys() - _merged.keys():
            _merged[remainder] += __merger_helper(remainder, acc) + curr[remainder]

        return _merged

    merged = reduce(__merge_duplicates, map(
        __count_symbol_occs_in_formula, tqdm(problem.premises, desc="calculating occ")
    ))

    seen = set()
    merged = Counter({
        symbol: merged[symbol] for symbol in merged.keys()
        if str(symbol) not in seen and not seen.add(str(symbol))
    })
    return merged


def __count_symbols_in_goal(problem: Problem) -> Counter:
    return __count_symbol_occs_in_formula(problem.conjecture)


def __get_symbols_in_formula(formula: Formula) -> set:
    return set(__count_symbol_occs_in_formula(formula).keys())


def __count_symbol_occs_in_formula(formula: Formula) -> Counter:
    def __inner(formula_: Formula) -> set:
        if isinstance(formula_, BinaryFormula):
            lhs_syms = __inner(formula_.left)
            rhs_syms = __inner(formula_.right)
            return lhs_syms.union(rhs_syms)

        elif isinstance(formula_, (FunctorExpression, PredicateExpression)):
            symbols = formula_.symbols()
            return set(symbols).union(__inner(
                formula_.arguments
            ))

        elif isinstance(formula_, UnaryFormula):
            symbols = set(formula_.symbols()).union(__inner(
                formula_.formula
            ))
            return symbols

        elif isinstance(formula_, QuantifiedFormula):
            symbols = set(formula_.symbols()).union(__inner(
                formula_.formula
            ))
            return symbols

        elif isinstance(formula_, (Constant, DefinedConstant)):
            return {formula_}

        elif isinstance(formula_, list):
            return set(itertools.chain.from_iterable(
                __inner(form) for form in formula_
            ))

        elif isinstance(formula_, Variable):
            return set()

        else:
            raise Exception(f"Unhandled type in {__count_symbol_occs_in_formula.__name__}: {type(formula_)}")

    return Counter(__inner(formula))


def __gather_axioms_using_triggers(trigger_relation: Iterable, symbols: Iterable) -> set:
    return {
        axiom for symbol in symbols for axiom, triggers in trigger_relation
        if any(str(symbol) == str(trigger) for trigger in triggers)
    }
