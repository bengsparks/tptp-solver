import itertools
import operator
from collections import Counter
from typing import Callable, Iterable

from gavel.dialects.db.structures import Formula
from gavel.logic.problem import Problem
from tqdm import tqdm

from __sine import __count_symbols_in_axioms, __count_symbol_occs_in_formula, __get_symbols_in_formula, \
    __gather_axioms_using_triggers


def sine(problem: Problem) -> Problem:
    return __sine(problem, __occ, __trigger)


def __occ(problem: Problem) -> Counter:
    return __count_symbols_in_axioms(problem)


def __trigger(problem: Problem, symbol_occs: dict) -> list:
    def __minimum_occ_symbol_in_formula(formula: Formula, _symbol_occs: dict) -> set:
        formula_symbol_occs = __count_symbol_occs_in_formula(formula)
        trigger_cands = {
            symbol: _symbol_occs[str(symbol)] for symbol in formula_symbol_occs.keys()
        }
        ordered = sorted(
            trigger_cands.items(), key=operator.itemgetter(1)
        )
        least_common = next(itertools.groupby(
            ordered,
            key=operator.itemgetter(1)
        ))

        extracted = set(map(
            operator.itemgetter(0), least_common[1]
        ))
        return extracted

    with_strings_as_keys = {
        str(symbol): occ for symbol, occ in symbol_occs.items()
    }
    return [
        (premise, __minimum_occ_symbol_in_formula(premise, with_strings_as_keys))
        for premise in tqdm(problem.premises, desc="calculating trigger")
    ]


def __sine(problem: Problem, occurrence_counter: Callable[[Problem], Counter],
           trigger: Callable[[Problem, dict], list]) -> Problem:
    occ = dict(sorted(
        occurrence_counter(problem).items(),
        key=operator.itemgetter(1)
    ))
    trigger_relation = trigger(problem, occ)

    symbols: Iterable = __get_symbols_in_formula(problem.conjecture)

    axioms = set()
    while True:
        old_size = len(axioms)
        axioms = axioms.union(__gather_axioms_using_triggers(
            trigger_relation, symbols
        ))
        new_size = len(axioms)
        if old_size == new_size:
            break

        symbols = __count_symbols_in_axioms(Problem(
            premises=axioms,
            conjecture=[]
        ))

    return Problem(
        premises=list(axioms),
        conjecture=problem.conjecture
    )
